#![allow(unused_imports)]
extern crate shellexpand;
extern crate expanduser;
extern crate clap;

#[allow(unused_imports)]
use std::fmt;
#[allow(unused_imports)]
use std::env;
#[allow(unused_imports)]
use std::convert::TryInto;
#[allow(unused_imports)]
use std::path::{ Path, PathBuf };
#[allow(unused_imports)]
use std::process;
// use std::fs::metadata as metadata;

#[allow(unused_imports)]
use clap::Parser;
// TODO: likely lot more practical seems flexi_logger (file, async/buff, dynamic spec)
#[allow(unused_imports)]
use env_logger;
#[allow(unused_imports)]
use log;
#[allow(unused_imports)]
use rand;

#[allow(dead_code)]
const DEFAULT_COLLECTION: &str = "~/Music";

#[derive(Parser)]
struct CliOpts {
    #[arg(short='t',long)]
    selftest: bool,
    collection: Option<String>,
    #[arg(short='v',long,action=clap::ArgAction::Count)]
    verbose: u8,
}

fn main() {
    println!("Hello world");

    let args = CliOpts::parse();

    let mut logger = env_logger::Builder::new();
    logger.filter_level(match args.verbose {
        0 => log::LevelFilter::Error,
        1 => log::LevelFilter::Warn,
        2 => log::LevelFilter::Debug,
        _ => log::LevelFilter::Trace,
    });
    logger.init();

    log::error!("Test Error message");
    println!("EndOf PlayOS");
}
