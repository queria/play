#![allow(dead_code)]

/*
possible improvements:
* clear up all leftover todo notes (esp those which got implemented already)
* there are few parts to investigate about memory allocation vs .path() calls
  (disable player spawning, point this to e.g. /usr and check mem
   and timings)
* write playlist to file
  (but after all other fs:: related parts, this is unlikely much
   interesting)
* possibly do scanning of individual artists in separate threads
  (input for work for each thread is likely easy,
   more interesting could be collecting good paths back eg via std::sync::mpsc
   or such - as also what would then happen with logging etc?)
* extra could be:
  - modules
  - crates (split to bin and library)
  - is there some use for generics in 'play' case
*/

extern crate clap;
extern crate expanduser;
extern crate shell_words;
extern crate shellexpand;

use std::fmt;
use std::env;
use std::convert::TryInto;
use std::path::{ Path, PathBuf };
use std::process;
use std::time;
// use std::fs::metadata as metadata;

use clap::Parser;
// TODO: likely lot more practical seems flexi_logger (file, async/buff, dynamic spec)
use env_logger;
use log;
use rand;

const DEFAULT_COLLECTION: &str = "~/Music";

const MEDIA_EXTS: [&str; 10] = ["aac", "avi", "flac", "m4a", "mp3", "mp4", "ogg", "opus", "webm", "wmv"]; // .map(|x| x.as_os_str()).collect();


const CMD_DEFAULT: &str = "mpv -vo=null --shuffle '{musicDir}'";
const CMD_PLACEHOLDER: &str = "{musicDir}";

const EMPTY_S: String = String::new();

#[derive(Parser)]
struct CliOpts {
    #[arg(short='t',long)]
    collection: Option<String>,
    /// Command for launching the music player with selected artist directory
    #[arg(short='p',long)]
    playercmd: Option<String>,
    #[arg(short='v',long,action=clap::ArgAction::Count)]
    verbose: u8,
}

fn main() {
    println!("***[ play-rust ]***");

    let args = CliOpts::parse();

    let mut logger = env_logger::Builder::new();
    logger.filter_level(match args.verbose {
        0 => log::LevelFilter::Error,
        1 => log::LevelFilter::Warn,
        2 => log::LevelFilter::Debug,
        _ => log::LevelFilter::Trace,
    });
    logger.init();

    let collection: PathBuf = get_collection_path(DEFAULT_COLLECTION, args.collection, env::var("COLLECTION").unwrap_or_default());
    log::info!("{:?}", &collection);
    if !is_dir(&collection) {
        eprintln!("ERROR: collection directory {} NOT FOUND!", collection.display());
        return;
    }

    println!("Scanning collection {}", collection.display());
    let authors = find_authors(&collection);
    log::debug!("{:?}", &authors);
    if authors.len() == 0 {
        eprintln!("ERROR: found no suitable music directories inside {}!", collection.display());
        return;
    }
    println!("Found {} available choices", authors.len());

    let selected = select_author(&authors);
    log::info!("{:?}", &selected);
    println!("Selected: {}", selected);

    play(&selected, &args.playercmd);

    header("END".to_string());
}

fn header(title: String) {
    println!("=========== {title} ==========");
}

fn play(selected_path: &String, playercmd_opt: &Option<String>) -> bool {

    let playercmd: Vec<String>;
    let mut playercmd_str: String = CMD_DEFAULT.to_string();

    let arg_replaced: bool;

    if let Some(pcmds) = playercmd_opt { playercmd_str = pcmds.to_owned(); }

    arg_replaced = playercmd_str.contains(CMD_PLACEHOLDER);
    let replaced = playercmd_str.replace(CMD_PLACEHOLDER, selected_path);
    match shell_words::split(&replaced) {
        Ok(pcmd) => playercmd = pcmd,
        Err(why) => { log::error!("ERROR: Unable to parse playercmd: [{:?}] {:?}", why, replaced); return false; },
    }

    let mut proc = process::Command::new(&playercmd[0]);
    proc.args(playercmd[1..].iter());
    if ! arg_replaced {
        proc.arg(selected_path);
    }

    println!("Trying to run: {:?}", proc);
    match proc.spawn() {
        Err(why) => {
            log::error!("Failed to start player: {}", why);
        },
        Ok(mut child) => {
            let ecode = child.wait();
            println!("Player exited: {:?}", ecode);
            match ecode {
                Err(why) => log::error!("Unknown exit status: {:?}", why),
                Ok(ecode) => return ecode.success(),
            }
        },
    }
    return false;
}

fn select_author(authors: &Vec<String>) -> String {
    /* TODO: select random index from authors */
    if authors.len() == 0 { return String::new(); }
    let idx = rand::random::<usize>() % authors.len();
    log::trace!("selecting: {} from {}", idx, authors.len());
    return String::from(&authors[idx])
}

fn find_authors(collection: &Path) -> Vec<String> {
    let mut authors: Vec<String> = Vec::new();
    let t_start = time::Instant::now();
    /* TODO: scan the path and find (suitable?) subdirs */
    match collection.read_dir() {
        Err(msg) => eprintln!("Unable to list collection: {:?} at {:?}", msg, collection),
        Ok(dir_content) => {
            for entry in dir_content {
                if let Ok(entry) = entry {
                    if is_valid_author(&entry.path()) {
                        authors.push(entry.path().display().to_string());
                    }
                }
            }
        }
    }
    log::info!("Time spent on scanning authors: {:?}", t_start.elapsed());
    //authors.push(collection.display().to_string());
    authors
}

fn is_valid_author(topdir: &Path) -> bool {
    if ! topdir.is_dir() { return false; }
    let mut dirs_to_scan: Vec<PathBuf> = Vec::new();
    dirs_to_scan.push(topdir.to_path_buf());
    /* TODO: maybe refactor scanning for directories to helper
     *       - needed in find_authors as also here
     *       - in both cases I dont care about what we cannot read (Err cases)
     *       - has to return iterator (here we exit on first match) - how to do one in rust?
     *       -- but already one safe/easy to use, without any Result/Option being returned
     *
     *       actually NOT, here we can process dirs and files in one go,
     *       no reason to do double processing of entries, lets learn iterators on something else
     */
    while let Some(dir) = dirs_to_scan.pop() {
        match dir.read_dir() {
            Err(msg) => eprintln!("Unable to list author: {:?} at {:?}", msg.to_string(), dir),
            Ok(dir_content) => {
                log::trace!("Scanning {} for music files ...", dir.display());
                for entry in dir_content {
                    if let Ok(entry) = entry {
                        /* TODO: retest speed - repeated .path() vs storing in variable
                         * (can be without extra allocation, or is it already?)  */
                        /* check media file first - can be cheaper then pushing to vec? */
                        if is_media_file(&entry.path()) {
                            return true;
                        } else if is_dir(&entry.path()) {
                            dirs_to_scan.push(entry.path());
                        }
                    }
                }
            }
        }
    }

    return false;
}

fn is_media_file(path: &Path) -> bool {
    if !path.is_file() { return false; }
    if let Some(ext) = path.extension() {
        let low_ext = ext.to_str().unwrap().to_ascii_lowercase();
        log::trace!("- file {} is maybe media file: {}", path.display(), &low_ext);
        return MEDIA_EXTS.contains(&low_ext.as_str());
    }

    return false;
}

fn is_dir(path: &Path) -> bool {
    match path.metadata() {
        Err(msg) => {
            log::debug!("is_dir({:?})): {:?}", path, msg);
            return false;
        },
        Ok(md) => {
            return md.is_dir();
        },
    }
}

fn get_collection_path(default: &str, cli_path: Option<String>, env_path: String) -> PathBuf {
    let mut col_path = default.to_string();
    if EMPTY_S != env_path {
        col_path = env_path;
    }
    col_path = cli_path.unwrap_or(col_path);


    /* beware - following parts are very fragile in regards to types and borrows, modify thoughtfully */
    log::trace!("before expand env: {}", col_path);
    match shellexpand::env(&col_path) {
        Ok(new_path) => col_path = new_path.to_string(),
        Err(msg) => log::debug!("get_collection_path/shellexpand({:?}): {:?}", &col_path, msg),
    }
    log::trace!("before expand user: {}", col_path);
    match expanduser::expanduser(&col_path) {
        Ok(new_path) => col_path = new_path.to_str().unwrap().to_string(),
        Err(msg) => log::debug!("get_collection_path/expanduser({:?}): {:?}", &col_path, msg),
    }
    log::trace!("{}", col_path);

    let mut p = PathBuf::new();
    p.push(col_path);
    p
    // default.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    const VALID_PATH: &str = "~/";
    const INVALID_PATH: &str = "~playrsWrongUser/";

    #[test]
    fn play_false() {
        let selected = String::new();
        assert!(!play(&selected, &Some("bash -c \"/bin/false\"".to_string())), "failed to execute /bin/false");
    }
    #[test]
    fn play_shell() {
        assert!(play(&String::new(), &Some("bash -c \"/bin/false || /bin/true\"".to_string())), "failed to execute /bin/true");
    }
    #[test]
    fn play_placeholder() {
        let selected = "/bin/true".to_string();
        assert!(play(&selected, &Some("bash -c \"{musicDir} || /bin/false\"".to_string())), "failed to execute with placeholder replacement");
    }
    #[test]
    fn play_mpv() {
        // here we expect that some music exists in default path
        let real_collection: PathBuf = get_collection_path(DEFAULT_COLLECTION, None, EMPTY_S);
        let authors = find_authors(&real_collection);
        let mut selected = select_author(&authors);
        println!("selected: {}\n", selected);

        selected = "--version".to_string();
        assert!(play(&selected, &None), "failed to execute mpv");
    }
    #[test]
    fn collection_path() {
        // test tilde expansion to home dir
        // assumes home exists ... which could be invalid though
        let collection: PathBuf = get_collection_path(VALID_PATH, None, EMPTY_S);
        assert!(is_dir(&collection), "user home should be expanded");

        // negative test tilde invalid user expansion
        let collection: PathBuf = get_collection_path(INVALID_PATH, None, EMPTY_S);
        assert!(!is_dir(&collection), "invalid user home should NOT be expanded");

        // test override via valid env var (of invalid default)
        let collection: PathBuf = get_collection_path(INVALID_PATH, None, VALID_PATH.to_string());
        assert!(is_dir(&collection), "invalid path should be overriden to valid by env var");

        // test override via valid cli param (of both invalid default and env var)
        let collection: PathBuf = get_collection_path(INVALID_PATH, Some(VALID_PATH.to_string()), INVALID_PATH.to_string());
        assert!(is_dir(&collection), "invalid path should be overriden to valid by env var");
    }

}









// Leftovers from learning rust basics/structs
#[allow(dead_code)]
fn age_ops() {
    header("Ops".to_string());
    let year = 2023u32;
    let birth = 1970u32;

    println!("Age is: {}", year - birth);
}

#[allow(dead_code)]
fn age_tuple() {
    header("Tuple".to_string());
    let now = (2023, 7, 5);
    let birth = (1970, 1, 1);

    let (y, m, d) = now;

    println!("{y} {m} {d}");
    println!("Now: {now:?}");
    println!("Birth: {birth:?}");

}

#[allow(dead_code)]
fn age_struct() {
    header("Struct".to_string());
    #[derive(Debug)]
    struct BadDate {
        year: u32,
        month: u8,
        day: u8,
    }

    impl fmt::Display for BadDate {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{}-{:0>2}-{:0>2}", self.year, self.month, self.day)
        }
    }

    impl BadDate {
        fn diff_days(&self, to: &BadDate) -> i32 {
            let years: i32 = (self.year - to.year).try_into().unwrap();
            let months: i32 = (self.month - to.month).try_into().unwrap();
            let days: i32 = (self.day - to.day).try_into().unwrap();
            (years * 365) + (months * 31) + days
        }
        fn diff_years(&self, to: &BadDate) -> f32 {
            (self.diff_days(to) as f32) / 365f32
        }
    }

    let now = BadDate { day: 25, year: 2023, month: 07 };
    let birth = BadDate { day: 1, month: 1, year: 1970 };
    println!("BDNow: {now:?} == {now}");
    println!("BDBirth: {birth}");

    println!("BDAgeDays: {}", now.diff_days(&birth));
    println!("BDAgeYears: {}", now.diff_years(&birth));
}
