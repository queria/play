# Play

*Project for my personal experiments - my 'play' project.*

Likely it is of no interest for almost anybody but me or my friends.

The *play* has two meanings here:
- to **play with random programming languages**
  - used it years ago to learn python
  - and refresh perl
  - later for go (that version is lost somewhere, maybe reason to write/refresh again)
  - and last is rust

- the target to implement is **music player** (actually wrapper)
  - it scans music collection
  - selects random artist from collection
  - and starts the actual player with songs of that selected artist
  - as this example *use-case* covers few interesting basic programming language topics:
    - cli parsing, formatting output
    - collections (array/list etc)
    - random number
    - filesystem handling (scanning, or possibly writing files)
    - process spawning
    - potential for lot more (tests, direct dependency on player lib without proc spawning, bin/lib split, logging and such)
  - although in practice i use lot [more basic bash alias](https://gitlab.com/queria/configs/-/blob/521d6e958db906a9ff9e7dfbc475c56a733b2c69/.bash_aliases#L88)
