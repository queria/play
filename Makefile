
RBINS = play-rust playos-rust

.PHONY: all
all: $(RBINS)

# for each binary, remap to source(dep) by matching to *-rust and adding .rs to rest
$(RBINS):%-rust:%.rs
	cargo build --release --bin $@
	cp target/release/$@ ./
# @echo === target is $@ with deps $<

# run devel loop to recompile and exec on file change
#.PHONY: dev-play-rust
#dev-play-rust:
#	QS_SKIPNOTIFY=1 runonchange play.rs bash -c 'clear; cargo test'

# no .PHONY due to using implicit pattern rule, workaround is usage of FORCE
## FORCE:
# supplements phony, as that cannot be used with implicit patterns/rules
# (it is less efficient than actual phony, but in this small scale it should not matter)
# [dev-play-rust, dev-playos ...] (dev-$binName)
# and then this works, but means loosing shell-completion
## dev-%: FORCE
# this works, but is statically defined
## dev-play-rust dev-playos: FORCE
# this works, by combining new list
# it's dynamically defined and even with phony (no static pattern rules, just multi target)
DEVBINS = $(foreach b, $(RBINS), dev-$(b))
.PHONY: $(DEVBINS)
$(DEVBINS):
	bash -c 'N=$@; N=$${N#dev-}; N=$${N%-rust}; export QS_SKIPNOTIFY=1; set -x; runonchange $$N.rs bash -c "clear; cargo test --bin $${N}-rust"'

.PHONY: clean
clean:
	cargo clean -v --release -p play-rust
	rm -vf $(RBINS)

.PHONY: purge
purge:
	cargo clean
	make clean
